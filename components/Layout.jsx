import React from 'react'
import Header from './Header'
import Footer from './Footer'
import Styles from '../Styles/Layout.module.css'
const Layout = ({children}) => {
  return (
    <>
        <Header/>
            <main className={Styles.main}>{children}</main>
        <Footer/>
    </>
  )
}

export default Layout