import React from 'react'
import Styles from '../Styles/Header.module.css'
import Image from 'next/image'
import { CgProfile } from "react-icons/cg";
const Header = () => {
  return (
    <header className={Styles.header}>
        <nav className={Styles.header__navigation}>
            <Image src={'/img/logo.png'} width={1024} height={1024} alt='logo'/>
            <div className={Styles.header__navigation__itemsLinks}>
                <span>Inicio</span>
                <span>Pedidos</span>
                <span>Servicio tecnico</span>
            </div>
            <CgProfile/>
        </nav>
        
    </header>
  )
}

export default Header