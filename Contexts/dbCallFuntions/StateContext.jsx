
import { context } from './createContext'
import { ReducerContext } from './ReducerContext'
import { useReducer } from 'react'
import axios from 'axios'
const StateContext = ({children}) => {
    const inicialState = {
        login: [],
        dataRead: []
    }
    const [state, dispatch] = useReducer(ReducerContext,inicialState)
    const root = '../api'
    async function fetchDataRead() {
        try {
          const response = await axios.get(`${root}/read`);
          const {data: data} = response
          dispatch({
            payload: data,
            type: 'dataRead',
            login: true
        })
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      }

      async function fetchDataCreate(formData) {
        try {
          const response = await axios.post(`${root}/create`, formData);
          console.log('Usuario creado con éxito:', response.data); 
        } catch (error) {
          console.error('Error al crear usuario:', error);
        }
      }
  
      async function fetchDataDelete(formData) {
        try {
          const response = await axios.post(`${root}/delete`, formData);
          console.log('Usuario borrar con éxito:', response.data); 
        } catch (error) {
          console.error('Error al borrar usuario:', error);
        }
      }

  return (
    <context.Provider value={{
        stateLogin: state.login,
        fetchDataRead,
        fetchDataCreate,
        fetchDataDelete
    }}
    >{children}
    </context.Provider>
  )
}

export default StateContext