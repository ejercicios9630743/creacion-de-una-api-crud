import pool from '../../db';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { username, targetColumn,newData } = req.body;
    const selectQuery = 'SELECT id FROM user WHERE e-mail = ?';
    const [rows] = await connection.query(selectQuery, username); //Debo recibir el e-mail iniciado
    try {
        if(rows.length > 0) {
            const id = rows.id
        }
      const connection = await pool.getConnection();
      //Se debe hacer una consulta dinamica recibiendo un objeto con las columna y valor a modificar
      const updateQuery = 'UPDATE user SET e-mail = ?, password = ? WHERE id = ?'; 
      const [result] = await connection.query(updateQuery, [newData.e-mail, newData.pass, id]);
      
      connection.release();
      
      if (result.affectedRows > 0) {
        res.status(200).json({ message: 'Registro actualizado correctamente' });
      } else {
        res.status(404).json({ message: 'Registro no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  } else {
    res.status(405).json({ message: 'Método no permitido' });
  }
}
