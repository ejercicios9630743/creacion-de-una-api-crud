import pool from '../../../db';

export default async function handler(req, res) {
    const {userEmail} = req.body
  try {
    const connection = await pool.getConnection();
    const [results] = await connection.query('DELETE FROM `user` WHERE `e-mail`= ?', userEmail);
    connection.release();
    res.status(200).json(results)
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}