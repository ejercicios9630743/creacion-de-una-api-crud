import pool from '../../../db';

export default async function handler(req, res) {
  try {
    const connection = await pool.getConnection();
    const [results] = await connection.query('SELECT * FROM user');
    connection.release();
    res.status(200).json(results);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}
