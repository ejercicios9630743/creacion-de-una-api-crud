import pool from '../../../db';

export default async function handler(req, res) {
    const {user: user,password: password} = req.body
  try {
    const connection = await pool.getConnection();
    const [results] = await connection.query('INSERT INTO `user`(`e-mail`, `password`, `rol`) VALUES (?, md5(?), ?)', [user, password, 'user']);
    connection.release();
    res.status(200).json(results)
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}