import StateContext from '@/Contexts/dbCallFuntions/StateContext'
import '../Styles/globals.css'
import {League_Spartan} from 'next/font/google'
const league_Spartan = League_Spartan({subsets: ['latin']})
// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <style jsx global>{`
        html {
          font-family: ${league_Spartan.style.fontFamily};
        }
      `}</style>
      <StateContext>
      <Component {...pageProps}/>
      </StateContext>
    </>
  )
}